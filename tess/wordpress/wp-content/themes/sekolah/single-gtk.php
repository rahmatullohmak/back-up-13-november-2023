<?php get_header(); ?>
    <!-- Single -->
	<?php 
	    // Single 
	    if (have_posts()):
		while (have_posts()): the_post(); 
			global $authordata, $post;
			$edu_guru = get_post_meta($post->ID, 'edu_guru', true);
			$his_guru = get_post_meta($post->ID, 'his_guru', true);
			$nik = get_post_meta($post->ID, '_nik', true);
	    	$nip = get_post_meta($post->ID, '_nip', true);
	    	$nuptk = get_post_meta($post->ID, '_nuptk', true);
			$mas = get_post_meta($post->ID, '_mas', true);
	        $sex = get_post_meta($post->ID, '_sex', true);
	    	$tmpt = get_post_meta($post->ID, '_tmpt', true);
	    	$tgl = get_post_meta($post->ID, '_tgl', true);
	    	$rel = get_post_meta($post->ID, '_rel', true);
		
	    	$add = get_post_meta($post->ID, '_add', true);
	    	$telp = get_post_meta($post->ID, '_telp', true);
	    	$waguru = get_post_meta($post->ID, '_waguru', true);
	    	$mail = get_post_meta($post->ID, '_mail', true);
	    	$akademifb = get_post_meta($post->ID, '_akademifb', true);
	    	$akademitw = get_post_meta($post->ID, '_akademitw', true);
	    	$akademiig = get_post_meta($post->ID, '_akademiig', true);
			
			$tangmas = date_i18n("d F Y", strtotime($mas));
			$tangtgl = date_i18n("d M Y", strtotime($tgl));
		?>
		
		<div class="sekolah_bread">
	    	<div class="sch_container">
	            <?php dimox_breadcrumbs(); ?>
        	</div>
    	</div>
		
		<section class="sch_singlepost people">
	        <div class="sch_container">
			
			    <div class="sch_singlecontent clear">
					
					<div class="sekolah_secondary">
						<div class="secondary_content">
						
						    <?php 
							    if (has_post_thumbnail()) {
									echo '<div class="sekolah_featured">'.get_the_post_thumbnail($post->ID, 'photo').'</div>'; 
								} else {
									echo '<div class="sekolah_featured"><img src="'. get_theme_file_uri('gambar/photo.jpg') .'" /></div>'; 
								}
							?>
							
			            </div>
					</div>
					
					<div class="sekolah_primary">
					    <div class="primary_content">
						
						    <?php the_title( '<h1 class="sekolah_heading">', '</h1>' ); ?>
							
							<table class="guru_table">
							    <tr>
							    	<td class="dpn"><strong><?php echo __( 'Aktif mulai', 'sekolah' ); ?></strong></td><td><?php if ( $mas != "" ) { echo esc_html( $tangmas ); } else { _e('-'); } ?></td>
								</tr>
								<tr>
							    	<td class="dpn"><strong><?php echo __( 'Jabatan', 'sekolah' ); ?></strong></td>
									<td>
									    <?php
										    $terms = get_the_terms( $post->ID , 'jab' );
							                if ( $terms != null ){
									        	foreach( $terms as $term ) {
										        	$term_link = get_term_link( $term, 'jab');
										        	echo '<a href="' . $term_link . '">' . $term->name . '</a> ';
										        	unset($term); 
									        	} 
								        	} 
										?>
									</td>
							    </tr>
								<tr>
								    <td class="dpn"><strong><?php echo __( 'Status', 'sekolah' ); ?></strong></td>
									<td>
									    <?php
										    $terms = get_the_terms( $post->ID , 'stts' );
							                if ( $terms != null ){
									        	foreach( $terms as $term ) {
										        	$term_link = get_term_link( $term, 'stts');
										        	echo '<a href="' . $term_link . '">' . $term->name . '</a> ';
										        	unset($term); 
									        	} 
								        	} 
										?>
									</td>
								</tr>
						    	<tr>
							    	<td class="dpn"><strong><?php echo __( 'Jenis Kelamin', 'sekolah' ); ?></strong></td><td><?php echo esc_html( $sex ); ?></td>
								</tr>
								<tr>
							    	<td class="dpn"><strong><?php echo __( 'T.T.L', 'sekolah' ); ?></strong></td><td><?php if ( $tmpt != "" ) { echo esc_html( $tmpt ); } else { _e('-'); } ?>, <?php if ( $tgl != "" ) { echo esc_html( $tangtgl ); } else { _e('-'); } ?> </td>
							    </tr>
								<tr>
								    <td class="dpn"><strong><?php echo __( 'Agama', 'sekolah' ); ?></strong></td><td><?php echo esc_html( $rel ); ?></td>
								</tr>
							    <tr>
							        <td class="dpn"><strong><?php echo __( 'NIK', 'sekolah' ); ?></strong></td><td><?php echo esc_html( $nik ); ?></td>
							    </tr>
								<tr>
							        <td class="dpn"><strong><?php echo __( 'NIP', 'sekolah' ); ?></strong></td><td><?php echo esc_html( $nip ); ?></td>
							    </tr>
							    <tr>
							    	<td class="dpn"><strong><?php echo __( 'NUPTK', 'sekolah' ); ?></strong></td><td><?php echo esc_html( $nuptk ); ?></td>
							    </tr>
								<?php if ( get_post_meta($post->ID, '_telp', true) !="" ) { ?>
						    	    <tr>
							    	    <td class="dpn"><strong><?php echo __( 'Telepon', 'sekolah' ); ?></strong></td><td><?php echo esc_html( $telp ); ?></td>
					                </tr>
						    	<?php } ?>
						    	<?php if ( get_post_meta($post->ID, '_waguru', true) !="" ) { ?>
						    		<tr>
								        <td class="dpn"><strong><?php echo __( 'Whatsapp', 'sekolah' ); ?></strong></td><td><?php echo esc_html( $waguru ); ?></td>
					                </tr>
						    	<?php } ?>
					    		<?php if ( get_post_meta($post->ID, '_mail', true) !="" ) { ?>
						    		<tr>
						    		    <td class="dpn"><strong><?php echo __( 'Email', 'sekolah' ); ?></strong></td><td><?php echo esc_html( $mail ); ?></td>
		    			            </tr>
			    				<?php } ?>
				    			<?php if ( get_post_meta($post->ID, '_akademifb', true) !="" ) { ?>
					       			<tr>
							    	    <td class="dpn"><strong><?php echo __( 'Facebook', 'sekolah' ); ?></strong></td><td><a target="_blank" href="<?php echo esc_html( $akademifb ); ?>">Facebook</a></td>
					                </tr>
	    						<?php } ?>
		    					<?php if ( get_post_meta($post->ID, '_akademitw', true) !="" ) { ?>
			    					<tr>
				    				    <td class="dpn"><strong><?php echo __( 'Twitter', 'sekolah' ); ?></strong></td><td><a target="_blank" href="<?php echo esc_html( $akademitw ); ?>">Twitter</a></td>
					                </tr>
						    	<?php } ?>
							    <?php if ( get_post_meta($post->ID, '_akademiig', true) !="" ) { ?>
					    			<tr>
					    			    <td class="dpn"><strong><?php echo __( 'Instagram', 'sekolah' ); ?></strong></td><td><a target="_blank" href="<?php echo esc_html( $akademiig ); ?>">Instagram</a></td>
					                </tr>
					    		<?php } ?>
							</table>
							
							<div class="clear content_meta">
							<?php echo __( 'Alamat : ', 'sekolah' ); ?><?php echo esc_html( $add ); ?>
							</div>
							<div class="sekolah_article">
							    <h3 class="art_title"><span><?php echo __( 'Profil Singkat', 'sekolah' ); ?></span></h3>
							    <?php 
									if ( !empty( get_the_content() ) ) {
										the_content();
									} else {
										echo __( 'Belum ada data', 'sekolah' );
									} 
								?>
							</div>
							
							<div class="after_art">
							    <h3 class="art_title"><span><?php echo __( 'Pendidikan', 'sekolah' ); ?></span></h3>
								<table class="guru_table">
								    <?php 
									    if (is_array($edu_guru)) {
											foreach ( $edu_guru as $edu ) { 
											?>  
											    <tr>
											    	<td class="dpn"><?php echo esc_html( $edu['eduname'] ); ?></td>
													<td><strong><?php echo esc_html( $edu['eduyear'] ); ?></strong></td>
												</tr>
											<?php 
											} 
										} else { 
										    ?>
											    <tr><td><?php echo __( 'Belum ada data', 'sekolah' ); ?></td></tr>
											<?php 
										} 
									?>
						    	</table>
							
							    <h3 class="art_title"><span><?php echo __( 'Pekerjaan', 'sekolah' ); ?></span></h3>
								<table class="guru_table">
								    <?php 
									    if (is_array($his_guru)) {
											foreach ( $his_guru as $his ) { 
											?>
							                    <tr>
											    	<td class="dpn"><?php echo esc_html( $his['hisname'] ); ?></td>
													<td><strong><?php echo esc_html( $his['hisyear'] ); ?></strong></td>
												</tr>
											<?php 
											} 
										} else { 
										    ?>
											    <tr><td><?php echo __( 'Belum ada data', 'sekolah' ); ?></td></tr>
											<?php 
										} 
									?>
						    	</table>
							</div>
							
						</div>
					</div>
					
				</div>
				
			</div>
		</section>
		
		<?php 
		endwhile; 
	    endif; 
	?>
	
<?php get_footer(); ?>