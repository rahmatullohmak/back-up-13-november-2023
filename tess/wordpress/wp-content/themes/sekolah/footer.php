           </div><!-- Content -->
			
		   <div class="sch-widget clear">
	    	   <div class="sch_container">
			       <div class="widget_area clear">
				       <div class="widget_box">
		                 	<div class="widget_box-inner">
			    	            <?php if ( is_active_sidebar( 'sidebar-1' ) ) {
						            dynamic_sidebar( 'sidebar-1' ); 
	                            } ?>
							</div>
						</div>
						<div class="widget_box">
							<div class="widget_box-inner">
							    <?php if ( is_active_sidebar( 'sidebar-2' ) ) {
									dynamic_sidebar( 'sidebar-2' );
								} ?>
							</div>
						</div>
						<div class="widget_box">
						    <div class="widget_box-inner">
								<?php if ( is_active_sidebar( 'sidebar-3' ) ) {
									dynamic_sidebar( 'sidebar-3' );
								} ?>
							</div>
						</div>
					</div>
				</div>
		    </div>
					
            <div class="footer">
		    	<div class="sch_container">
			    	<div class="copyright">
					    <?php sekolah_text_footer(); ?>
			    	</div>
		    	</div>
			</div><!-- footer --> 
			
        </div><!-- sekolah -->   
		
		<span class="to_top"><i class="fa fa-chevron-up"></i></span>
				
		<?php wp_footer(); ?>
		
        <script type="text/javascript">
		
		    function fetch(){
				jQuery.ajax({
					url: '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>',
					type: 'post',
					data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
					success: function(data) {
						jQuery('#datafetch').html( data );
					}
				});
			}
			
			function sisfetch(){
				jQuery.ajax({
					url: '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>',
					type: 'post',
					data: { action: 'siswa_fetch', student: jQuery('#student').val() },
					success: function(data) {
						jQuery('#siswafetch').html( data );
					}
				});
			}
			
			jQuery('.menu-icon').click(function() {
				jQuery('.navigasi').slideToggle('slow');
			});
			
			function resize() {
                if (jQuery(window).width() < 983) {
					jQuery('.navigasi .sch').addClass('accord').removeClass('desktop');
                } else {
	                jQuery('.navigasi .sch').removeClass('accord').addClass('desktop');
	            }
            }

            jQuery(document).ready( function() {
                jQuery(window).resize(resize);
                resize();
            });

            jQuery(document).ready( function() {
                jQuery('ul.accord').accordion();
            });
			
			jQuery('.to_top').click(function() {
			    jQuery('html, body').animate({scrollTop: '0px'}, 1000);
			});
			
			jQuery(document).bind("scroll", function() { 
				if( jQuery(this).scrollTop() >= 350) {
					jQuery('body').addClass("fade");
				} else {
					jQuery('body').removeClass("fade");
				}
			});
			jQuery(function(){
                jQuery("ul#schticker").liScroll();
            });
		</script>
	</body>
</html>