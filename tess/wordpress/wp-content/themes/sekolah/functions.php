<?php
/**
 * Tema WP sekolah menambahkan beberapa fungsi hook
 * Memanggil beberapa fungsi lain, dibawah ini
 */

/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'flush_rewrite_rules' );

if ( version_compare( $GLOBALS['wp_version'], '5.5', '<' ) ) {
	// WP sekolah baiknya dijalankan pada WordPress 5.5 atau lebih tinggi.
	require get_template_directory() . '/tambahan/back-compat.php';
}

if ( ! function_exists( 'sekolah_setup' ) ) :
    function sekolah_setup() {
		$color_scheme             = sekolah_get_color_scheme();
		$default_background_color = trim( $color_scheme[0], '#' );
		add_theme_support(
		'custom-background',
		apply_filters(
			'roooda_custom_background_args',
			array(
				'default-color' => $default_background_color,
			)
		)
    	);
    	
		// custom logo
		add_theme_support( 'custom-logo', array(
	    	'height'      => 240,
	    	'width'       => 600,
	    	'flex-height' => true,
    	) );
		
	    add_theme_support('post-thumbnails');
	    add_image_size('slider', 1200, 800, true);
    	add_image_size('news', 600, 450, true);
    	add_image_size('photo', 300, 400, true);
    	add_image_size('plite', 110, 147, true);
    	add_image_size('gall', 160, 120, true);
    	add_image_size('small', 80, 60, true);
		
		// menambahkan menu navigasi
		register_nav_menus(array(
	    	'navigation' => __('Tampilkan Menu ini di Navigasi Header', 'sekolah'),
    	));
		add_theme_support('html5', array(
	    	'search-form', 'comment-form', 'comment-list',
    	));
		
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'responsive-embeds' );
		add_theme_support( 'title-tag' );
	
	    
    }

endif; // sekolah_setup
add_action( 'after_setup_theme', 'sekolah_setup' );


function sekolah_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sekolah_content_width', 840 );
}
add_action( 'after_setup_theme', 'sekolah_content_width', 0 );

/*
 * Register widget (sidebar).
 */
function sekolah_widgets_init() {
	require get_template_directory().'/widgets/posts.php';
	register_widget('Recentposts');
	require get_template_directory().'/widgets/school.php';
	register_widget('SchoolInfo');
	require get_template_directory().'/widgets/agenda.php';
	register_widget('RecentAgenda');
	require get_template_directory().'/widgets/pengumuman.php';
	register_widget('RecentPengumuman');
	require get_template_directory().'/widgets/blog.php';
	register_widget('RecentBlog');
	require get_template_directory().'/widgets/videos.php';
	register_widget('Videos');

	register_sidebar(array(
		'name' => __('Footer 1', 'sekolah'),
		'id'   => 'sidebar-1',
		'before_widget' => '<div id="%1$s" class="widget_block %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	
	register_sidebar(array(
		'name' => __('Footer 2', 'sekolah'),
		'id'   => 'sidebar-2',
		'before_widget' => '<div id="%1$s" class="widget_block %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));

	register_sidebar(array(
		'name' => __('Footer 3', 'sekolah'),
		'id'   => 'sidebar-3',
		'before_widget' => '<div id="%1$s" class="widget_block %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	
}
add_action('widgets_init', 'sekolah_widgets_init');

/**
 *
 * Hide Admin on Frontend
 */
 
show_admin_bar(false);

function return_30( $seconds ) {
  // change the default feed cache recreation period to 30 seconds;
  return 120;
}
add_filter( 'wp_feed_cache_transient_lifetime' , 'return_30' ); 
 
/**
 * Register script
 */
 
function sekolah_stylescripts() {
	$theme_version = wp_get_theme()->get( 'Version' );
	// panggil css
	$refresh = date_i18n("His");
	// panggil css
	wp_enqueue_style('sekolah-style', get_stylesheet_uri(), array(), $refresh );
	wp_enqueue_style('sekolah-owl', get_template_directory_uri().'/stylesheet/owl.carousel.min.css', array(), $theme_version );
	wp_enqueue_style('sekolah-ani', get_template_directory_uri().'/stylesheet/owl.animate.css', array(), $theme_version );
	wp_enqueue_style('sekolah-theme', get_template_directory_uri().'/stylesheet/owl.theme.default.min.css', array(), $theme_version );
	wp_enqueue_style('sekolah-awe', get_template_directory_uri().'/fontawesome/css/font-awesome.css', array(), $theme_version );
}
add_action('wp_enqueue_scripts', 'sekolah_stylescripts');

function sekolah_scripts() {
	// panggil script
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'sekolah-owls', get_template_directory_uri() . '/javascript/owl.carousel.min.js', array(), false, true );
	wp_enqueue_script( 'sekolah-ticker', get_template_directory_uri() . '/javascript/newstickers.js', array(), false, true );
	wp_enqueue_script( 'sekolah-accordion', get_template_directory_uri() . '/javascript/accordion.js', array(), false, true );
	
}
add_action('wp_enqueue_scripts', 'sekolah_scripts');

/**
 * Custom template untuk tema WP sekolah.
 */
require get_template_directory() . '/tambahan/template-tags.php';
require get_template_directory() . '/tambahan/customizer.php';
require get_template_directory() . '/tambahan/partialrefresh.php';

/**
 * Panel untuk Customizer.
 */
require get_template_directory() . '/tambahan/coloring.php';
require get_template_directory() . '/tambahan/color-option.php';
require get_template_directory() . '/tambahan/color-enqueue.php';
require get_template_directory() . '/tambahan/color-print.php';
require get_template_directory() . '/tambahan/color-print-footer.php';
require get_template_directory() . '/tambahan/color-inline-css.php';

/**
 * Daftarkan post type.
 */
require get_template_directory() . '/post-khusus/post-khusus.php';
require get_template_directory() . '/tambahan/func/breadcrumb.php';
require get_template_directory() . '/tambahan/func/ciuss-news.php';
require get_template_directory() . '/tambahan/func/create-info.php';

if (is_admin() && isset($_GET['activated']) && $pagenow == 'themes.php') {
	update_option('posts_per_page', 12);
}

function sekolah_numeric_pagination() {

	if( is_singular() )
		return;
	global $wp_query;

	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );
	if ( $paged >= 1 )
		$links[] = $paged;
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';
		printf( '<a%s href="%s">%s</a>' . "\n", esc_html( $class ), esc_url( get_pagenum_link( 1 ) ), '1' );
		if ( ! in_array( 2, $links ) )
			echo ' … ';
	}

	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<a%s href="%s">%s</a>' . "\n", esc_html( $class ), esc_url( get_pagenum_link( $link ) ), esc_html( $link ));
	}

	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo ' … ' . "\n";
		$class = $paged == $max ? ' class="active"' : '';
		printf( '<a%s href="%s">%s</a>' . "\n", esc_html( $class ), esc_url( get_pagenum_link( $max ) ), esc_html( $max ));
	}

}

function shapeSpace_popular_posts($post_id) {
	$count_key = 'popular_posts';
	$count = get_post_meta($post_id, $count_key, true);
	if ($count == '') {
		$count = 0;
		delete_post_meta($post_id, $count_key);
		add_post_meta($post_id, $count_key, '0');
	} else {
		$count++;
		update_post_meta($post_id, $count_key, $count);
	}
}
function shapeSpace_track_posts($post_id) {
	if (!is_single()) return;
	if (empty($post_id)) {
		global $post;
		$post_id = $post->ID;
	}
	shapeSpace_popular_posts($post_id);
}
add_action('wp_head', 'shapeSpace_track_posts');

function getPostViews($postID){ 
    $count_key = 'post_views_count'; 
	$count = get_post_meta($postID, 
	$count_key, true); 
	
	if($count==''){ 
	delete_post_meta($postID, $count_key); 
	add_post_meta($postID, $count_key, '0'); return "0 View"; 
	} 
	return $count; 
} 
	
function setPostViews($postID) { 
    $count_key = 'post_views_count'; 
	$count = get_post_meta($postID, $count_key, true); 
	
	if($count==''){ 
	$count = 0; delete_post_meta($postID, $count_key); 
	add_post_meta($postID, $count_key, '0'); 
	}else{ 
	$count++; 
	update_post_meta($postID, $count_key, $count); 
	} 
}

function sekolah_event_expired($post_ID) {
	    global $post;
        $post_event_date = get_post_meta($post_ID, '_tevent', true);
		$end = get_post_meta($post_ID, '_tevent', true).' '.get_post_meta($post_ID, '_jam', true);
		$exp = strtotime(date_i18n($end));
		$dday = strtotime(date_i18n('d-m-Y H:i'));
		$sisa = $exp-$dday;
		$event_date = date("d F Y", strtotime($post_event_date));
        if ($post_event_date != "" ) {
            echo esc_html( $event_date );
			if ( $sisa < 0 ) { 
			    echo '<br/><strong class="expired">';
				esc_html_e( 'Acara sudah lewat', 'sekolah' ); 
				echo '</strong>';
			} else {
				echo '<br/><strong class="next">';
				esc_html_e( 'Acara segera berlangsung', 'sekolah' ); 
				echo '</strong>';
			}
        }
}

function sekolah_event_columns($defaults) {
        $defaults['sekolah_expired'] = esc_html_e( 'Pelaksanaan', 'sekolah' );
        return $defaults;
}
	
function event_columns_content($column_name, $post_ID) {
        if ($column_name == 'sekolah_expired') {
			global $post;
            $post_expired_event = sekolah_event_expired($post_ID);
			$end = get_post_meta($post_ID, '_tevent', true).' '.get_post_meta($post_ID, '_jam', true);
	    	$exp = strtotime(date_i18n($end));
	    	$dday = strtotime(date_i18n('d-m-Y H:i'));
	    	$sisa = $exp-$dday;
			if ($post_expired_event != "" ) {
                echo esc_html( $post_expired_event );
				if ( $sisa < 0 ) { 
				echo '<br/><strong>';
				esc_html_e( 'Acara sudah lewat', 'sekolah' ); 
				echo '</strong>';
				}
            }
        }
}
	
add_filter('manage_event_posts_columns', 'sekolah_event_columns', 10);
add_action('manage_event_posts_custom_column', 'event_columns_content', 10, 2);

function new_excerpt_length($length) {
	return 200;
}
add_filter('excerpt_length', 'new_excerpt_length');

function smart_excerpt($string, $limit) {
	$words = explode(" ", $string);
	if (count($words) >= $limit) 
		$dots = '..';
	else 
		$dots = '';
	echo esc_html( implode(" ", array_splice($words, 0, $limit)).$dots );
}

function commentslist($comment, $args, $depth) { ?>
	
	    <div class="clear">
		    <div class="comment__meta">
				<?php if ($comment->comment_approved == '0'): ?>
					<p><?php esc_html_e('Komentar Anda menunggu disetujui admin.', 'sekolah') ?></p>
					<br/>
				<?php 
				endif;
				    echo '<div class="comment__author"><span>';
					echo get_comment_author_link();
					echo '</span></div>';
					echo '<div class="comment__author"><span>';
					echo esc_html( get_comment_date('l, j M Y'));
					echo '</span></div>';
					comment_text();
					comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) 
				?>
			</div>
		</div>
<?php
}

function comments_link_attributes() {
	return 'class="comments_popup_link"';
}
add_filter('comments_popup_link_attributes', 'comments_link_attributes');

function next_posts_attributes() {
	return 'class="nextpostslink"';
}
add_filter('next_posts_link_attributes', 'next_posts_attributes');

function prev_posts_attributes() {
	return 'class="previouspostslink"';
}
add_filter('previous_posts_link_attributes', 'prev_posts_attributes');

$exl_posts = array();

add_action('admin_head', 'sekolah_admin_styles');

function sekolah_admin_styles() { ?>
	<style>
	    <?php 
		    $user = wp_get_current_user();
            $allowed_roles = array( 'guru', 'master', 'operator', 'siswa' );
            if ( array_intersect( $allowed_roles, $user->roles ) ) { ?>
            
			.user-rich-editing-wrap,
			.user-admin-color-wrap,
			.show-admin-bar,
			.user-comment-shortcuts-wrap,
			.show-admin-bar user-admin-bar-front-wrap,
			.user-language-wrap,
			.user-profile-picture,
			#application-passwords-section {
				display: none;
		    }
			<?php 
			} 
		?>
		.sekolah_metabox label {
			display: block;
			margin: 8px 0;
		}
		.clear:after {
            content: ".";
			display: block;
			height: 0;
			clear: both;
			visibility: hidden;
		}
		.sekolah_metabox table {
			width: 100%;
		}
		.half {
			width: calc( 50% - 40px );
			float: left;
		}
		.halfclose {
			width: 80px;
			float: left;
		}
		.halfin {
			padding: 0 12px 8px 0;
		}
		.sekolah_hidden {
			position: relative;
		}
		.sekolah_hidden:after {
			content: "";
			position: absolute;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			z-index: 10;
			background: #fff;
			opacity: 0.4;
		}
		.family {
			width: 50%; float: left;
		}
		.infam {
			padding: 0 15px 0 0;
		}
		.rss-box {
			margin: -12px -12px 12px;
		}
		.rss-box img {
			width: 100%;
			height: auto;
		}
		.rss-footer {
			margin: 12px -12px 0;
			padding: 12px 12px 0;
			border-top: 1px solid #ddd; 
		}
		.sekolah_metabox .full {
			width: 100%;
		}
		.sekolah_metabox .abcd {
			width: 25%;
			float: left;
		}
		.button-move {
			background: #f33;
		}
		.premium {
			display:block; 
			border-radius: 4px; 
			font-style: normal; 
			border: 1px solid #ddd; 
			background: #fff; 
			padding: 10px 15px;
		}
		
		@media screen and (max-width:800px) {
		.rss-web {
			display: none;
		}
		.rss-mob {
			display: inline-block;
		}
		} 
		
	</style>
<?php }

/**
 * Shortcode.
 */
require get_template_directory() . '/tambahan/func/shortcode.php';

// Pencarian Siswa Ajax
add_action('wp_ajax_siswa_fetch' , 'siswa_fetch');
add_action('wp_ajax_nopriv_siswa_fetch','siswa_fetch');

function siswa_fetch(){
	if (isset($_POST['student'])) {
    $the_query = new WP_Query( 
        array( 
            's' => sanitize_text_field( wp_unslash( $_POST['student'] ) ), 
            'post_type' => 'siswa' 
        ) 
    );
	}

    if( $the_query->have_posts() ) :
	    echo '<table class="result_siswa">';
		echo '<tr><td>Nama</td><td>Kelas</td></tr>';
        while( $the_query->have_posts() ): $the_query->the_post();
		    global $post;
			$nisn = get_post_meta($post->ID, '_nisn', true);
		    $myquery = sanitize_text_field( wp_unslash( $_POST['student'] ) );
			$a = $myquery;
			$search = get_the_title();
			if( stripos("/{$search}/", $a) !== false) { ?>
			
			    <tr>
                    <td><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></td>
					<td>
					    <?php 
					        $terms = get_the_terms( $post->ID , 'kelas' );
							if ( $terms != null ){
								foreach( $terms as $termkelas ) {
									echo esc_html( $termkelas->name );
									unset($termkelas); 
								} 
							} 
						?>
					</td>
				</tr>
				
            <?php }
		endwhile;
		echo '</table>';
        wp_reset_postdata();  
    endif;

    die();
}


// the ajax function
add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){

	if (isset($_POST['keyword'])) {
    $the_query = new WP_Query( 
        array( 
            's' => sanitize_text_field( wp_unslash( $_POST['keyword'] ) ), 
            'post_type' => 'download' 
        ) 
    );
	}

    if( $the_query->have_posts() ) :
        while( $the_query->have_posts() ): $the_query->the_post();
		    global $post;
			$strFile = get_post_meta($post -> ID, $key = 'podcast_file', true);
			$sizeFile = get_post_meta($post -> ID, $key = 'size_file', true);
			 
		    $myquery = sanitize_text_field( wp_unslash( $_POST['keyword']) );
			$a = $myquery;
			$search = get_the_title();
			if( stripos("/{$search}/", $a) !== false) { ?>
			
			    <div class="ajax_down clear">
				    <div class="ajax_box">
                    <h4><?php the_title();?></h4>
					<?php 
					    esc_html_e('File', 'sekolah'); ?> <?php $terms = get_the_terms( $post->ID , 'type-file' );
						if ( $terms != null ){
							foreach( $terms as $typefile ) {
								echo esc_html( $typefile->name );
								unset($typefile); 
							} 
						} 
					?> <?php esc_html_e('ukuran', 'sekolah'); ?> <?php echo esc_html( $sizeFile ); ?>
				</div>
				
				<a class="down_down" download="<?php echo esc_attr( $strFile ); ?>" href="<?php echo esc_attr( $strFile ); ?>"><i class="fa fa-download"></i></a>
				</div>
            <?php }
		endwhile;
        wp_reset_postdata();  
    endif;

    die();
}

function custom_post_author_archive($query) {
    if ($query->is_author)
        $query->set( 'post_type', array('blog') );
    remove_action( 'pre_get_posts', 'custom_post_author_archive' );
}
add_action('pre_get_posts', 'custom_post_author_archive'); 

/**
 * Role User.
 */
require get_template_directory() . '/tambahan/func/capabilities.php';

function only_show_user_images( $query ) {
	$current_userID = get_current_user_id();
	if ( $current_userID && !current_user_can('manage_options')) {
		$query['author'] = $current_userID;
	}
	return $query; 
}

add_filter( 'ajax_query_attachments_args', 'only_show_user_images' ); 

function sekolah_rest_change_media_context( $result, $server, $request ) {
		// apply only to GET method
		$method = $request->get_method();
		if ( 'GET' !== $method ) {
			return $result;
		}

		$url = $request->get_route();
		if ( ! is_user_logged_in() || ( false === strpos( $url, '/wp/v2/media/' ) ) ) {
			return $result;
		}

		if ( 'edit' === $request->get_param( 'context' ) ) {
			$request->set_param( 'context', 'view' );
		}

		return $result;
	}

add_filter( 'rest_pre_dispatch', 'sekolah_rest_change_media_context', 10, 3 );

function post_publish_after() {
	
	global $post;
	
	$date = get_post_time('G', true, $post);
	
	
	// Array of time period chunks
	$chunks = array(
		array( 60 * 60 * 24 * 365 , __( 'tahun', 'sekolah' ), __( 'tahun', 'sekolah' ) ),
		array( 60 * 60 * 24 * 30 , __( 'bulan', 'sekolah' ), __( 'bulan', 'sekolah' ) ),
		array( 60 * 60 * 24 * 7, __( 'minggu', 'sekolah' ), __( 'minggu', 'sekolah' ) ),
		array( 60 * 60 * 24 , __( 'hari', 'sekolah' ), __( 'hari', 'sekolah' ) ),
		array( 60 * 60 , __( 'jam', 'sekolah' ), __( 'jam', 'sekolah' ) ),
		array( 60 , __( 'menit', 'sekolah' ), __( 'menit', 'sekolah' ) ),
		array( 1, __( 'detik', 'sekolah' ), __( 'detik', 'sekolah' ) )
	);

	if ( !is_numeric( $date ) ) {
		$time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
		$date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
		$date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
	}
	
	$current_time = current_time( 'mysql', $gmt = 7 ); // waktu Indonesia GMT + 7
	$newer_date = strtotime( $current_time );

	// Jeda waktu dalam detik
	$since = $newer_date - $date;

	// Perhitungan waktu tak tentu.
	if ( 0 > $since )
		return __( 'beberapa', 'sekolah' );

	//Step one
	for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
		$seconds = $chunks[$i][0];

		if ( ( $count = floor($since / $seconds) ) != 0 )
			break;
	}

	// Set output var
	$output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
	

	if ( !(int)trim($output) ){
		$output = '0 ' . __( 'detik', 'sekolah' );
	}
	
	$output .= __(' yang lalu', 'sekolah');
	
	return $output;
}

add_filter('the_time', 'post_publish_after');

if( current_user_can( 'siswa' ) ) {
    add_filter( 'show_password_fields', '__return_false' );
} 
