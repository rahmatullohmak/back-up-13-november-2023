	<form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
		<div class="ps_input">
		    <input name="s" type="text" placeholder="<?php echo __( 'Cari sesuatu...', 'sekolah' ); ?>" value="" />
		</div>
		<div class="ps_select">
			<select name="post_type">
	    		<option value="post"><?php echo __( 'Pos', 'sekolah' ); ?></option>
				<option value="editorial"><?php echo __( 'Editorial', 'sekolah' ); ?></option>
				<option value="blog"><?php echo __( 'Blog', 'sekolah' ); ?></option>
				<option value="pengumuman"><?php echo __( 'Pengumuman', 'sekolah' ); ?></option>
				<option value="agenda"><?php echo __( 'Agenda', 'sekolah' ); ?></option>
				<option value="fasilitas"><?php echo __( 'Fasilitas', 'sekolah' ); ?></option>
				<?php if ( get_theme_mod('sekolah_type') != "sekolah" ) { ?>
				    <option value="ekskul"><?php echo __( 'Ekskul', 'sekolah' ); ?></option>
				<?php } else { ?>
				    <option value="ukm"><?php echo __( 'Unit Kegiatan', 'sekolah' ); ?></option>
				<?php } ?>
				<option value="prestasi"><?php echo __( 'Prestasi', 'sekolah' ); ?></option>
				
			</select>
		</div>
		<div class="ps_button">
			<button type="submit"><i class="fa fa-search"></i></button>
		</div>
	</form>
