<?php
class RecentBlog extends WP_Widget {
	function __construct() {
		parent::__construct(
			'recentblog',
			esc_html__( 'WP Sekolah : Blog Guru', 'sekolah' ),
			array( 'description' => esc_html__( 'Widget ini digunakan untuk menampilkan Pos Blog Guru', 'sekolah' ), 'customize_selective_refresh' => true, )
		);
	}

	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Blog Guru' );

		/** This filter is documented in wp-tambahans/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		
		$blog_guru = ( ! empty( $instance['blog_guru'] ) ) ? absint( $instance['blog_guru'] ) : 3;
		if ( ! $blog_guru ) {
			$blog_guru = 3;
		}
		
		$q_args = array( 
			'post_type' => 'blog', 
			'numberposts' => $blog_guru,
			); 
			
		global $post;
		$blogpost = get_posts($q_args);
		
		echo $args['before_widget']; 
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		echo '<ul class="wp-block-latest-posts wp-block-latest-posts__list">';
		foreach ($blogpost as $post):
			setup_postdata($post);
		?>
        <li>
		    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br />
			<time><em><?php echo __( 'Oleh : ', 'sekolah'); ?><?php the_author(); ?></em></time>
		</li>
		<?php	
		endforeach;
		echo '</ul>';
		echo $args['after_widget'];
	}


	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['blog_guru'] = sanitize_text_field( $new_instance['blog_guru'] );
		return $instance;
	}
		
		
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'Blog Guru';
		$blog_guru     = isset( $instance['blog_guru'] ) ? esc_attr( $instance['blog_guru'] ) : 10; ?>
		<p><?php _e('Widget ini digunakan untuk menampilkan daftar Blog Guru terbaru di sidebar'); ?><br/></p>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title :' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id( 'blog_guru' ); ?>"><?php _e( 'Jumlah pos :' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'blog_guru' ); ?>" name="<?php echo $this->get_field_name( 'blog_guru' ); ?>" type="number" value="<?php echo $blog_guru; ?>" /></p>

    <?php
	}
}