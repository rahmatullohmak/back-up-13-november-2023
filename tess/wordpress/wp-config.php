<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tess' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'JI38>5klZI);)}ZVtuyl?LYl@Lj%gr2SBYYhI-;[`W_P`A@AAo;y6yX{hI_KZ0/j' );
define( 'SECURE_AUTH_KEY',  '=X?y=/-{&VwY>9zWsYAWs9%S^,`&w3:#;zQf|&kG14-exUQ,rEzpueu5K}j.*48r' );
define( 'LOGGED_IN_KEY',    'rM4wv!Og+SC=iK[p5nMZagd4IN:XiLbq==&h~YOd[SD,OTWTmc<;#J5gO7/g0]+F' );
define( 'NONCE_KEY',        'DUF,v{v*ObuBw.oi>Jz*jT%ul}<d%Wpf;t^A((UWudbE,Ge0iHv-lt>VinJg0KV]' );
define( 'AUTH_SALT',        ' ,JX&P3~3@Cq^$VC.9NWV|6d-|4i@#@E$3#0%Y)ZS4ea_+Ze:6O*LH2=#k$bD!k;' );
define( 'SECURE_AUTH_SALT', 'G?!di;@K;`zsXkSk*UT,S]s;FS=3A^YJ(xlqtqUY=s!h[52Dz,SvB/k|*ZRXRQt*' );
define( 'LOGGED_IN_SALT',   'T5W=l#vrP1L]u?=c;J!1-^;X<bgy,FWL30Ec[%a6aZlV!+e^Xj6,.XF52:_[hG2s' );
define( 'NONCE_SALT',       'SKbHp!y<qBEo,ls*&~S5qR:?atk];.q4~)%X7?ol>,S!E3QQL<0GfOFV>6.n;{m_' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_tess';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
