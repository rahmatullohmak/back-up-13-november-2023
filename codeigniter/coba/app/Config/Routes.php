<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

$routes->get('/porto', 'portofolio::index/$1');
$routes->get('/portofolio/about/(:any)', 'portofolio::about/$1');
$routes->get('/portofolio/contact', 'portofolio::contact');
$routes->get('/portofolio/blogs', 'portofolio::blogs');

$routes->get('/komic', 'komic::table');
