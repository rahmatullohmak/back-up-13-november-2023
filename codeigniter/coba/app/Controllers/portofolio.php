<?php

namespace App\Controllers;

class portofolio extends BaseController
{
    public function index($ind)
    {


        $data = ["title" => "$ind"];
        echo view('portofolio/lyout/head', $data);
        echo view('portofolio/lyout/main');
        echo view('portofolio/lyout/control');
    }
    public function about($ind)
    {
        $data = ["title" => "$ind"];
        echo view('portofolio/lyout/head', $data);
        echo view('portofolio/lyout/main');
        echo view('portofolio/lyout/isi/about');
        echo view('portofolio/lyout/control');
    }
    public function blogs()
    {
        $data = ['title' => 'blogs'];
        echo view('portofolio/lyout/head', $data);
        echo view('portofolio/lyout/main');
        echo view('portofolio/lyout/isi/blogs');
        echo view('portofolio/lyout/control');
    }
    public function contact()
    {
        $data = ['title' => 'contack'];
        echo view('portofolio/lyout/head', $data);
        echo view('portofolio/lyout/main');
        echo view('portofolio/lyout/isi/contact');
        echo view('portofolio/lyout/control');
    }
}
