<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'web' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=au&a]4fkgcIYGSwg1eI*no/c~:@O_D$ZYxGG,t|]Hi$ wqwkZ8jj,$t+D6(TFfX' );
define( 'SECURE_AUTH_KEY',  '1Eenuwu!kNoNO;FD`EE$Yu`e#f`qkqjQZ-4dk7cG:i;]zOPw;|q9Bqz1}n)Fq7 s' );
define( 'LOGGED_IN_KEY',    'QvYbWioN%$7t,@y.VJp}`u{?Mps],:;4B7Man?/<f^k8oVc*t{wRbA,{*I-wF:a5' );
define( 'NONCE_KEY',        'B^<^~-(lAq2Y2Z^B.MKO,Qraf*+(I&ukH,?_Z*gpE=XuB5hirO0/#ZgOUnu:TzGp' );
define( 'AUTH_SALT',        'dIX61ZPty[h*~.?XS ^|]ivFyvUY]whk({ftO]PJJ,]&MzZO].Q@Kd4okoca.EE.' );
define( 'SECURE_AUTH_SALT', 'Z@927Yygm$zTL4gW&g>pr1:wPTaP[KfU9k $;s8`f[]sTY1K;*a ]=P0RdLEKsV&' );
define( 'LOGGED_IN_SALT',   ')!ekEm_d!IO.,_Ec*B@8[Luh_Kj_.WzswUrl#+;.N%irzL|J(:d[EZgjsv4@kf(P' );
define( 'NONCE_SALT',       'AAkr*G]T1-P%D@V0E5&H8P=bJd@xF[ok{h3.kMNK)i]SOGa[^ONy1H>%+h3RF%JO' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
