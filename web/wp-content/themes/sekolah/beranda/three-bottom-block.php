<div class="sch_container">
    <div class="editornews clear">
	
	    <div class="gal-box">
		    <div class="news-inner"><h3><span class="re_kegiatan"><?php sekolah_re_kegiatan(); ?></span></h3></div>
			<script>
            jQuery(document).ready(function($) {
                var owl = $('.ekslide');
                owl.owlCarousel({
                    loop: true,
                    nav: false,
					dots: false,
                    lazyLoad: true,
			    	autoplay: true,
                    autoplayTimeout: 5000,
                    smartSpeed: 1000,
                    autoplayHoverPause: true,
                    margin: 15,
			    	responsive:{
                        0:{ 
				    	    items:2
                        },
                        640:{
                            items:4
                        },
                        1025:{
                            items:1
                        }
                    } 
                });
            });
            </script>
	    	<?php 
			    if ( get_theme_mod('sekolah_type') != "akademi" ) {
					$ekskul = 'ekskul';
				} else {
					$ekskul = 'ukm';
				}
				// query
		    	$args = array(
			        'post_type' => $ekskul,
			        'posts_per_page' => 5, 
		        );
	    	    query_posts($args);
				?>
				
				<div class="inner-gal clear">
			    	<div class="sesuatu">
					    <div class="ekslide owl-carousel owl-theme">
					    <?php
					    	if (have_posts()) {
								while (have_posts()): the_post(); 
								?>
								
								<div class="item">
							    	<?php 
								    	if (has_post_thumbnail()) { 
										?>
						                	<a href="<?php the_permalink() ?>" class="news_featured">
										    	<?php the_post_thumbnail('news'); ?>
											</a>
										<?php } else { ?>
									    	<a href="<?php the_permalink() ?>" class="news_featured">
										    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/gambar/sekolah.jpg"/>
											</a>
											<?php 
										} 
									?>
								</div>
								<?php 
						    	endwhile;
					    	} 
						?>
					    </div>
					</div>
				</div>
				
				<?php
		    	wp_reset_query(); 
	    	?>
    	</div>

		<div class="gal-box">
		    <div class="news-inner"><h3><span class="re_fasilitas"><?php sekolah_re_fasilitas(); ?></span></h3></div>
	    	<?php // query
		    	$args = array(
			        'post_type' => 'fasilitas', 
			        'posts_per_page' => 3, 
					'orderby' => 'rand',
		        );
	    	    query_posts($args);
				?>
				
				<div class="inner-gal clear">
				
				<?php
				if (have_posts()) {
					while (have_posts()): the_post(); 
					?>
				    <div class="galtext clear">
						<div class="gal-in clear">
					    	<?php 
							    if (has_post_thumbnail()) { 
								?>
						        	<a href="<?php the_permalink() ?>" class="news_featured">
										<?php 
											the_post_thumbnail('gall', array(
												'alt' => trim(strip_tags($post->post_title)),
											    'title' => trim(strip_tags($post->post_title)),
											)); 
										?>
								    </a>
							    <?php } else { ?>
								    <a href="<?php the_permalink() ?>" class="news_featured">
			    	    		        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/gambar/sekolah.jpg"/>
			        			    </a>
							    <?php 
								} 
							?>
							<div class="fas-right">
						    	<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
								<div class="editor-text"><?php echo esc_html( smart_excerpt(get_the_excerpt(), 8 )); ?></div>
							</div>
						</div>
					</div>
					
							
					<?php 
					endwhile;
				} 
				?>
				
				</div>
				
				<?php
			wp_reset_query(); 
		?>
    	</div>
			    
        <div class="gal-box">
		    <div class="news-inner"><h3><span class="re_galeri"><?php sekolah_re_galeri(); ?></span></h3></div>
	    	<?php // query
		    	$args = array(
			        'post_type' => 'galeri', 
			        'posts_per_page' => 9, 
					'orderby' => 'rand',
		        );
	    	    query_posts($args);
				?>
				
				<div class="inner-gal clear">
				
				<?php
				if (have_posts()) {
					while (have_posts()): the_post(); 
					?>
				    	<div class="galbox clear">
						    <div class="gal-in">
					    	<?php 
							    if (has_post_thumbnail()) { 
								?>
						        	<a href="<?php the_permalink() ?>" class="news_featured">
										<?php 
											the_post_thumbnail('gall', array(
												'alt' => trim(strip_tags($post->post_title)),
											    'title' => trim(strip_tags($post->post_title)),
											)); 
										?>
								    </a>
							    <?php } else { ?>
								    <a href="<?php the_permalink() ?>" class="news_featured">
			    	    		        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/gambar/sekolah.jpg"/>
			        			    </a>
							    <?php 
								} 
							?>
							</div>
						</div>
					
							
					<?php 
					endwhile;
				} 
				?>
				
				</div>
				
				<?php
			wp_reset_query(); 
		?>
    	</div>
			    
		
	</div>
</div>