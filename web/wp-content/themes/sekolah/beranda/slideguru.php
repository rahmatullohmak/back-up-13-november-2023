<div class="sch_container">
	<div class="guruku clear">
	    <div class="guru-judul"><span class="re_gurus"><?php sekolah_re_gurus(); ?></span></div>
		<script>
            jQuery(document).ready(function($) {
                var owl = $('.person');
                owl.owlCarousel({
                    loop: true,
                    nav: true,
					dots: false,
                    lazyLoad: true,
			    	autoplay: true,
                    autoplayTimeout: 3000,
                    smartSpeed: 1500,
                    autoplayHoverPause: true,
                    margin: 15,
			    	responsive:{
                        0:{ 
				    	    items:2
                        },
                        640:{
                            items:4
                        },
                        1025:{
                            items:8
                        }
                    }   
                });
            });
        </script>
		<?php if ( get_theme_mod('sekolah_type') == "akademi" ) {
			$gurus = 'dosen';
		} else if ( get_theme_mod('sekolah_type') == "pesantren" ) {
			$gurus = 'ustadz';
		} else {
			$gurus = 'gtk';
		} ?>
		
		<?php 
	    	$args = array(
			    'post_type' => $gurus, 
			    'posts_per_page' => 15, 
			);
		    query_posts($args);
	    	?>	
			
			<div class="person owl-carousel owl-theme">
			    
				<?php if (have_posts()) {
					while (have_posts()): the_post(); 
					?>	
                    <div class="item clear">
					    <a href="<?php the_permalink(); ?>">
					    	<?php // Featured Image
						        if (has_post_thumbnail()) { 
					    	        the_post_thumbnail('photo'); 					
					    	    } else {
						    	    echo '<img src="'.esc_url( get_theme_file_uri('gambar/guru.jpg') ).'" />';	
					    	    }
				    	    ?>	
						</a>
					</div>
					<?php endwhile;
					} 
				?>
			</div>
			
		<?php wp_reset_query(); ?>
	</div>
</div>