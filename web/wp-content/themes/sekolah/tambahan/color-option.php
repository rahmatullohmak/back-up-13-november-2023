<?php
function sekolah_get_color_schemes() {
	return apply_filters(
		'sekolah_color_schemes',
		array(
			'default' => array(
				'label'  => __( 'Default', 'sekolah' ),
				'colors' => array(
					'#fdfdfd', // sekolah body background 0
					'#ffffff', // sekolah wrapper background 1
					'#222222', // sekolah text global color 2
					'#45b767', // sekolah link global color 3
					'#45b767', // sekolah menu background 4
					'#222222', // sekolah menu aksen color 5 
					'#ffffff', // sekolah menu link color 6
					'#ffffff', // sekolah running bg 7
					'#666666', // sekolah running color 8
					'#ffffff', // sekolah header bg color 9
					'#45b767', // sekolah header text color 10
					'#666666', // sekolah header link color 11
					'#45b767', // sekolah header sosial bg 12					'#ffffff', // sekolah header sosial color 13					'#45b767', // sekolah bg nama sekolah 14					'#ffffff', // sekolah nama sekolah 15					'#ffffff', // sekolah editorial bg 16					'#444444', // sekolah editorial color 17					'#45b767', // sekolah editorial link 18					'#45b767', // sekolah home quotes bg 19					'#ffffff', // sekolah home quotes color 20					'#f7f7f7', // sekolah home slider gtk bg 21					'#222222', // sekolah home slider gtk color 22					'#ffffff', // sekolah peng agenda blog bg 23					'#222222', // sekolah peng agenda blog color 24					'#45b767', // sekolah peng agenda blog link 25					'#f7f7f7', // sekolah ekskul home bg 26					'#222222', // sekolah ekskul home color 27					'#45b767', // sekolah ekskul home link 28					'#ffffff', // sekolah video home bg 29					'#222222', // sekolah video home color 30					'#45b767', // sekolah widget footer bg 31					'#ffffff', // sekolah widget footer color 32					'#ffffff', // sekolah widget footer link 33					'#45b767', // sekolah footer bg color 34					'#ffffff', // sekolah footer text color 35					'#ffffff', // sekolah footer link color 36				),			),			'dark' => array(				'label'  => __( 'Dark Mode', 'sekolah' ),				'colors' => array(					'#171717', // sekolah body background 0					'#000000', // sekolah wrapper background 1					'#dddddd', // sekolah text global color 2					'#dddddd', // sekolah link global color 3					'#000000', // sekolah menu background 4					'#000000', // sekolah menu aksen color 5 					'#dddddd', // sekolah menu link color 6					'#222222', // sekolah home quotes bg 7					'#ffffff', // sekolah running color 8					'#222222', // sekolah header bg color 9					'#ffffff', // sekolah header text color 10					'#ffffff', // sekolah header link color 11					'#ffffff', // sekolah header sosial bg 12					'#222222', // sekolah header sosial color 13					'#111111', // sekolah bg nama sekolah 14					'#dddddd', // sekolah nama sekolah 15					'#000000', // sekolah editorial bg 16					'#dddddd', // sekolah editorial color 17					'#aaaaaa', // sekolah editorial link 18					'#000000', // sekolah home quotes bg 19					'#dddddd', // sekolah home quotes color 20					'#111111', // sekolah home slider gtk bg 21					'#dddddd', // sekolah home slider gtk color 22					'#000000', // sekolah peng agenda blog bg 23					'#dddddd', // sekolah peng agenda blog color 24					'#aaaaaa', // sekolah peng agenda blog link 25					'#111111', // sekolah ekskul home bg 26					'#dddddd', // sekolah ekskul home color 27					'#aaaaaa', // sekolah ekskul home link 28					'#111111', // sekolah video home bg 29					'#dddddd', // sekolah video home color 30					'#000000', // sekolah widget footer bg 31					'#dddddd', // sekolah widget footer color 32					'#aaaaaa', // sekolah widget footer link 33					'#111111', // sekolah footer bg color 34					'#ffffff', // sekolah footer text color 35					'#dddddd', // sekolah footer link color 36				),			),
			'purple' => array(
				'label'  => __( 'Purple', 'sekolah' ),
				'colors' => array(
					'#fdfdfd', // sekolah body background 0
					'#ffffff', // sekolah wrapper background 1
					'#222222', // sekolah text global color 2
					'#8224e3', // sekolah link global color 3
					'#8224e3', // sekolah menu background 4
					'#222222', // sekolah menu aksen color 5 
					'#ffffff', // sekolah menu link color 6
					'#ffffff', // sekolah running bg 7
					'#666666', // sekolah running color 8
					'#ffffff', // sekolah header bg color 9
					'#8224e3', // sekolah header text color 10
					'#666666', // sekolah header link color 11
					'#8224e3', // sekolah header sosial bg 12
					'#ffffff', // sekolah header sosial color 13
					'#8224e3', // sekolah bg nama sekolah 14
					'#ffffff', // sekolah nama sekolah 15
					'#ffffff', // sekolah editorial bg 16
					'#444444', // sekolah editorial color 17
					'#8224e3', // sekolah editorial link 18
					'#8224e3', // sekolah home quotes bg 19
					'#ffffff', // sekolah home quotes color 20
					'#f7f7f7', // sekolah home slider gtk bg 21
					'#222222', // sekolah home slider gtk color 22
					'#ffffff', // sekolah peng agenda blog bg 23
					'#222222', // sekolah peng agenda blog color 24
					'#8224e3', // sekolah peng agenda blog link 25
					'#f7f7f7', // sekolah ekskul home bg 26
					'#222222', // sekolah ekskul home color 27
					'#8224e3', // sekolah ekskul home link 28
					'#ffffff', // sekolah video home bg 29
					'#222222', // sekolah video home color 30
					'#8224e3', // sekolah widget footer bg 31
					'#ffffff', // sekolah widget footer color 32
					'#ffffff', // sekolah widget footer link 33
					'#8224e3', // sekolah footer bg color 34
					'#ffffff', // sekolah footer text color 35
					'#ffffff', // sekolah footer link color 36
				),
			),
			'blue' => array(
				'label'  => __( 'Blue', 'sekolah' ),
				'colors' => array(
					'#fdfdfd', // sekolah body background 0
					'#ffffff', // sekolah wrapper background 1
					'#222222', // sekolah text global color 2
					'#1e73be', // sekolah link global color 3
					'#1e73be', // sekolah menu background 4
					'#222222', // sekolah menu aksen color 5 
					'#ffffff', // sekolah menu link color 6
					'#ffffff', // sekolah running bg 7
					'#666666', // sekolah running color 8
					'#ffffff', // sekolah header bg color 9
					'#1e73be', // sekolah header text color 10
					'#666666', // sekolah header link color 11
					'#1e73be', // sekolah header sosial bg 12
					'#ffffff', // sekolah header sosial color 13
					'#1e73be', // sekolah bg nama sekolah 14
					'#ffffff', // sekolah nama sekolah 15
					'#ffffff', // sekolah editorial bg 16
					'#444444', // sekolah editorial color 17
					'#1e73be', // sekolah editorial link 18
					'#1e73be', // sekolah home quotes bg 19
					'#ffffff', // sekolah home quotes color 20
					'#f7f7f7', // sekolah home slider gtk bg 21
					'#222222', // sekolah home slider gtk color 22
					'#ffffff', // sekolah peng agenda blog bg 23
					'#222222', // sekolah peng agenda blog color 24
					'#1e73be', // sekolah peng agenda blog link 25
					'#f7f7f7', // sekolah ekskul home bg 26
					'#222222', // sekolah ekskul home color 27
					'#1e73be', // sekolah ekskul home link 28
					'#ffffff', // sekolah video home bg 29
					'#222222', // sekolah video home color 30
					'#1e73be', // sekolah widget footer bg 31
					'#ffffff', // sekolah widget footer color 32
					'#ffffff', // sekolah widget footer link 33
					'#1e73be', // sekolah footer bg color 34
					'#ffffff', // sekolah footer text color 35
					'#ffffff', // sekolah footer link color 36
				),
			),
			'green' => array(
				'label'  => __( 'Green', 'sekolah' ),
				'colors' => array(
					'#fdfdfd', // sekolah body background 0
					'#ffffff', // sekolah wrapper background 1
					'#222222', // sekolah text global color 2
					'#2a893d', // sekolah link global color 3
					'#2a893d', // sekolah menu background 4
					'#222222', // sekolah menu aksen color 5 
					'#ffffff', // sekolah menu link color 6
					'#ffffff', // sekolah running bg 7
					'#666666', // sekolah running color 8
					'#ffffff', // sekolah header bg color 9
					'#2a893d', // sekolah header text color 10
					'#666666', // sekolah header link color 11
					'#2a893d', // sekolah header sosial bg 12
					'#ffffff', // sekolah header sosial color 13
					'#2a893d', // sekolah bg nama sekolah 14
					'#ffffff', // sekolah nama sekolah 15
					'#ffffff', // sekolah editorial bg 16
					'#444444', // sekolah editorial color 17
					'#2a893d', // sekolah editorial link 18
					'#2a893d', // sekolah home quotes bg 19
					'#ffffff', // sekolah home quotes color 20
					'#f7f7f7', // sekolah home slider gtk bg 21
					'#222222', // sekolah home slider gtk color 22
					'#ffffff', // sekolah peng agenda blog bg 23
					'#222222', // sekolah peng agenda blog color 24
					'#2a893d', // sekolah peng agenda blog link 25
					'#f7f7f7', // sekolah ekskul home bg 26
					'#222222', // sekolah ekskul home color 27
					'#2a893d', // sekolah ekskul home link 28
					'#ffffff', // sekolah video home bg 29
					'#222222', // sekolah video home color 30
					'#2a893d', // sekolah widget footer bg 31
					'#ffffff', // sekolah widget footer color 32
					'#ffffff', // sekolah widget footer link 33
					'#2a893d', // sekolah footer bg color 34
					'#ffffff', // sekolah footer text color 35
					'#ffffff', // sekolah footer link color 36
				),
			),
			'red' => array(
				'label'  => __( 'Red', 'sekolah' ),
				'colors' => array(
					'#fdfdfd', // sekolah body background 0
					'#ffffff', // sekolah wrapper background 1
					'#222222', // sekolah text global color 2
					'#dd3333', // sekolah link global color 3
					'#dd3333', // sekolah menu background 4
					'#222222', // sekolah menu aksen color 5 
					'#ffffff', // sekolah menu link color 6
					'#ffffff', // sekolah running bg 7
					'#666666', // sekolah running color 8
					'#ffffff', // sekolah header bg color 9
					'#dd3333', // sekolah header text color 10
					'#666666', // sekolah header link color 11
					'#dd3333', // sekolah header sosial bg 12
					'#ffffff', // sekolah header sosial color 13
					'#dd3333', // sekolah bg nama sekolah 14
					'#ffffff', // sekolah nama sekolah 15
					'#ffffff', // sekolah editorial bg 16
					'#444444', // sekolah editorial color 17
					'#dd3333', // sekolah editorial link 18
					'#dd3333', // sekolah home quotes bg 19
					'#ffffff', // sekolah home quotes color 20
					'#f7f7f7', // sekolah home slider gtk bg 21
					'#222222', // sekolah home slider gtk color 22
					'#ffffff', // sekolah peng agenda blog bg 23
					'#222222', // sekolah peng agenda blog color 24
					'#dd3333', // sekolah peng agenda blog link 25
					'#f7f7f7', // sekolah ekskul home bg 26
					'#222222', // sekolah ekskul home color 27
					'#dd3333', // sekolah ekskul home link 28
					'#ffffff', // sekolah video home bg 29
					'#222222', // sekolah video home color 30
					'#dd3333', // sekolah widget footer bg 31
					'#ffffff', // sekolah widget footer color 32
					'#ffffff', // sekolah widget footer link 33
					'#dd3333', // sekolah footer bg color 34
					'#ffffff', // sekolah footer text color 35
					'#ffffff', // sekolah footer link color 36
				),
			),
			'white' => array(
				'label'  => __( 'White Clean', 'sekolah' ),
				'colors' => array(
					'#fdfdfd', // sekolah body background 0
					'#ffffff', // sekolah wrapper background 1
					'#222222', // sekolah text global color 2
					'#2a893d', // sekolah link global color 3
					'#f7f7f7', // sekolah menu background 4
					'#777777', // sekolah menu aksen color 5 
					'#555555', // sekolah menu link color 6
					'#ffffff', // sekolah running bg 7
					'#666666', // sekolah running color 8
					'#ffffff', // sekolah header bg color 9
					'#2a893d', // sekolah header text color 10
					'#666666', // sekolah header link color 11
					'#2a893d', // sekolah header sosial bg 12
					'#ffffff', // sekolah header sosial color 13
					'#ffffff', // sekolah bg nama sekolah 14
					'#2a893d', // sekolah nama sekolah 15
					'#fdfdfd', // sekolah editorial bg 16
					'#444444', // sekolah editorial color 17
					'#2ca7d3', // sekolah editorial link 18
					'#f7f7f7', // sekolah home quotes bg 19
					'#777777', // sekolah home quotes color 20
					'#ffffff', // sekolah home slider gtk bg 21
					'#dd3333', // sekolah home slider gtk color 22
					'#ffffff', // sekolah peng agenda blog bg 23
					'#222222', // sekolah peng agenda blog color 24
					'#8224e3', // sekolah peng agenda blog link 25
					'#f7f7f7', // sekolah ekskul home bg 26
					'#222222', // sekolah ekskul home color 27
					'#777777', // sekolah ekskul home link 28
					'#f7f7f7', // sekolah video home bg 29
					'#222222', // sekolah video home color 30
					'#ffffff', // sekolah widget footer bg 31
					'#555555', // sekolah widget footer color 32
					'#888888', // sekolah widget footer link 33
					'#f7f7f7', // sekolah footer bg color 34
					'#555555', // sekolah footer text color 35
					'#8224e3', // sekolah footer link color 36
				),
			),		)	);}