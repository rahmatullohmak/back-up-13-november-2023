<?php
class SchoolInfo extends WP_Widget {
	function __construct() {
		parent::__construct(
			'infosekolah',
			esc_html__( 'WP Sekolah : Info Sekolah', 'sekolah' ),
			array( 'description' => esc_html__( 'Widget ini digunakan untuk menampilkan Info Sekolah', 'sekolah' ), 'customize_selective_refresh' => true, )
		);
	}

	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Info Sekolah' );
		$schmaps = ( ! empty( $instance['mapsembed'] ) ) ? '<div class="embed_found">'.$instance['mapsembed'].'</div>' : '';

		/** This filter is documented in wp-tambahans/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		
		echo $args['before_widget']; 
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		?>
		    <div class="widget_sekolah clear">
			    <div class="sch_maps"><?php echo $schmaps; ?></div>
			    <h4 class="nama_sekolah"><?php nama_sekolah(); ?></h4>
				
				<table class="widget_contact">
				    <tr>
				    	<td><?php echo __('NSPN :', 'sekolah'); ?></td>
						<td><span class="nspn"><?php sekolah_nspn(); ?></td>
					</tr>
				    <?php if ( get_theme_mod('address_data') != "" ) { ?>
				    <tr>
				        <td colspan="2"><span class="address"><?php alamat_sekolah(); ?></span></td>
					</tr>
					<?php } ?>
					<?php if ( get_theme_mod('telpon_data') != "" ) { ?>
					<tr>
					    <td><?php _e('TELEPON'); ?></td>
						<td class="schright"><?php echo get_theme_mod('telpon_data'); ?></td>
					</tr>
					<?php } ?>
					<?php if ( get_theme_mod('email_data') != "" ) { ?>
					<tr>
					    <td><?php _e('EMAIL'); ?></td>
						<td class="schright"><?php echo get_theme_mod('email_data'); ?></td>
					</tr>
					<?php } ?>
					<?php if ( get_theme_mod('wa_data') != "" ) { ?>
					<tr>
					    <td><?php _e('WHATSAPP'); ?></td>
						<td class="schright"><?php echo get_theme_mod('wa_data'); ?></td>
					</tr>
					<?php } ?>
				</table>
			</div>
    	<?php	
		
		echo $args['after_widget'];
	}


	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['mapsembed'] = $new_instance['mapsembed'];
		return $instance;
	}
		
		
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'Info Sekolah'; 
		$schmaps   = isset( $instance['mapsembed'] ) ? '<div class="embed_found">'.esc_attr( $instance['mapsembed'] ).'</div>' : ''; 
		?>
		<p><?php _e( 'Widget ini digunakan untuk menampilkan daftar Maps Sekolah di sidebar' ); ?><br/></p>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Judul Widget' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
		<p><label for="embedmaps"><?php _e( 'Masukan kode embed Maps' ); ?></label>
		<textarea class="widefat" id="<?php echo $this->get_field_id( 'mapsembed' ); ?>" name="<?php echo $this->get_field_name( 'mapsembed' ); ?>"><?php echo $schmaps; ?></textarea></p>
	
    <?php
	}
}