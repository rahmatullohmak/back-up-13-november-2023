<?php
class RecentAgenda extends WP_Widget {
	function __construct() {
		parent::__construct(
			'recentagenda',
			esc_html__( 'WP Sekolah : Agenda', 'sekolah' ),
			array( 'description' => esc_html__( 'Widget ini digunakan untuk menampilkan Post Agenda', 'sekolah' ), 'customize_selective_refresh' => true, )
		);
	}

	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Agenda' );

		/** This filter is documented in wp-tambahans/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		
		$agenda_total = ( ! empty( $instance['agenda_total'] ) ) ? absint( $instance['agenda_total'] ) : 3;
		if ( ! $agenda_total ) {
			$agenda_total = 3;
		}
		$today = strtotime(date('d-m-Y'));
		$q_args = array( 
			'post_type' => 'agenda', 
			'numberposts' => $agenda_total,
			'meta_key' => '_minus',
	    	'meta_query' => array(
			array(
				'key' => '_minus',
				'compare' => '>='
				)
			),
	    	'orderby' => 'meta_value',
	    	'order' => 'DESC'
	    	); 
			
		global $post;
		$eventpost = get_posts($q_args);
		
		echo $args['before_widget']; 
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		echo '<ul class="wp-block-latest-posts wp-block-latest-posts__list">';
		foreach ($eventpost as $post):
			setup_postdata($post);
			$tanggalan = get_post_meta($post->ID, '_tevent', true);
	    	$minus = strtotime(get_post_meta($post->ID, '_tevent', true));
	    	$jam = get_post_meta($post->ID, '_jam', true);
		    $thisday = strtotime(date_i18n('d-m-Y H:i'));
			$end = get_post_meta($post->ID, '_tevent', true).' '.get_post_meta($post->ID, '_jam', true);
			$exp = strtotime(date_i18n($end));
			$sisa = $exp-$thisday;
			
			if ( $sisa > 0 ) { ?>
            <li>
				<span><?php echo date_i18n('d M Y', strtotime(esc_html($tanggalan))); ?><?php _e(' / waktu :'); ?> <?php echo esc_html($jam); ?></span><br/>
				<h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
			</li>  
			<?php } else { ?>
			<li>
				<span><?php _e('Acara sudah lewat'); ?> <del><?php echo date_i18n('d M Y', strtotime(esc_html($tanggalan))); ?></del></span><br/>
				<h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
			</li>
			<?php }
			endforeach;
		    echo '</ul>';
		echo $args['after_widget'];
	}


	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['agenda_total'] = sanitize_text_field( $new_instance['agenda_total'] );
		return $instance;
	}
		
		
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'Agenda';
		$agenda_total     = isset( $instance['agenda_total'] ) ? esc_attr( $instance['agenda_total'] ) : 3; ?>
		<p><?php _e('Widget ini digunakan untuk menampilkan daftar Agenda terbaru di sidebar') ?><br/></p>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title :' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id( 'agenda_total' ); ?>"><?php _e( 'Jumlah pos :' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'agenda_total' ); ?>" name="<?php echo $this->get_field_name( 'agenda_total' ); ?>" type="number" value="<?php echo $agenda_total; ?>" /></p>

    <?php
	}
}