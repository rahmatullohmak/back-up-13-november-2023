<?php
class Videos extends WP_Widget {
	function __construct() {
		parent::__construct(
			'sekolah_widvideo',
			esc_html__( 'WP Sekolah : Video Terbaru', 'sekolah' ),
			array( 'description' => esc_html__( 'Widget ini digunakan untuk menampilkan Post Video Terbaru', 'sekolah' ), 'customize_selective_refresh' => true, )
		);
	}

	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html_e( 'Video Terbaru', 'sekolah' );

		/** This filter is documented in wp-tambahans/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		
		$vidpost = ( ! empty( $instance['vidpost'] ) ) ? absint( $instance['vidpost'] ) : 3;
		if ( ! $vidpost ) {
			$vidpost = 3;
		}
		
		$q_args = array( 
			'post_type' => 'video', 
			'numberposts' => $vidpost,
			); 
			
		global $post;
		$videopost = get_posts($q_args);
		
		
		echo $args['before_widget']; 
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		echo '<div class="wp-block-video">';
		foreach ($videopost as $post):
		    $vidembed = get_post_meta($post->ID, '_vidembed', true); 
			setup_postdata($post);
		?>
		    <figure>
                <iframe src="<?php echo esc_url( 'https://www.youtube.com/embed/'. rawurlencode( $vidembed ) .'' ); ?>" frameborder="0" allowfullscreen></iframe>
			</figure>
		<?php	
		endforeach;
		echo '</div>';
		
		echo $args['after_widget'];
	}


	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['vidpost'] = sanitize_text_field( $new_instance['vidpost'] );
		return $instance;
	}
		
		
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'Video Terbaru';
		$vidpost     = isset( $instance['vidpost'] ) ? esc_attr( $instance['vidpost'] ) : 3; ?>
		<p><?php _e('Widget ini digunakan untuk menampilkan daftar Video Terbaru terbaru di sidebar'); ?><br/></p>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title :' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id( 'vidpost' ); ?>"><?php _e( 'Jumlah pos :' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'vidpost' ); ?>" name="<?php echo $this->get_field_name( 'vidpost' ); ?>" type="number" value="<?php echo $vidpost; ?>" /></p>

    <?php
	}
}