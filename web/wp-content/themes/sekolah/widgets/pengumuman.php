<?php
class RecentPengumuman extends WP_Widget {
	function __construct() {
		parent::__construct(
			'recentpengumuman',
			esc_html__( 'WP Sekolah : Pengumuman', 'sekolah' ),
			array( 'description' => esc_html__( 'Widget ini digunakan untuk menampilkan Post Pengumuman', 'sekolah' ), 'customize_selective_refresh' => true, )
		);
	}

	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Pengumuman' );

		/** This filter is documented in wp-tambahans/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		
		$sekolah_peng = ( ! empty( $instance['sekolah_peng'] ) ) ? absint( $instance['sekolah_peng'] ) : 3;
		if ( ! $sekolah_peng ) {
			$sekolah_peng = 3;
		}
		
		$q_args = array( 
			'post_type' => 'pengumuman', 
			'numberposts' => $sekolah_peng,
			); 
			
		global $post;
		$pengpost = get_posts($q_args);
		
		echo $args['before_widget']; 
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		echo '<ul class="wp-block-latest-posts wp-block-latest-posts__list">';
		foreach ($pengpost as $post):
			setup_postdata($post);
		?>
		<li>
		    <time><em><?php printf(__('Diterbitkan : <span class="post-date">%s</span>', 'sekolah'), get_the_date()); ?></em></time><br/>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br />
			<time><?php if (function_exists('smart_excerpt')) smart_excerpt(get_the_excerpt(), 15); ?></time>
		</li>
		<?php	
		endforeach;
		echo '</ul>';
		echo $args['after_widget'];
	}


	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['sekolah_peng'] = sanitize_text_field( $new_instance['sekolah_peng'] );
		return $instance;
	}
		
		
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'Pengumuman';
		$sekolah_peng     = isset( $instance['sekolah_peng'] ) ? esc_attr( $instance['sekolah_peng'] ) : 3; ?>
		<p><?php _e( 'Widget ini digunakan untuk menampilkan daftar Pengumuman terbaru di sidebar'); ?><br/></p>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title :' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id( 'sekolah_peng' ); ?>"><?php _e( 'Jumlah pos :' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'sekolah_peng' ); ?>" name="<?php echo $this->get_field_name( 'sekolah_peng' ); ?>" type="number" value="<?php echo $sekolah_peng; ?>" /></p>

    <?php
	}
}